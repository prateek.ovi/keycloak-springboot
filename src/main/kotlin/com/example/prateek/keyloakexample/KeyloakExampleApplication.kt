package com.example.prateek.keyloakexample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KeyloakExampleApplication

fun main(args: Array<String>) {
	runApplication<KeyloakExampleApplication>(*args)
}
